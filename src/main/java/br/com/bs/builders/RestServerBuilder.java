/*
 * BlueShift intelectual property.
 * Contact the BlueShift team before use this source.
 * http://blueshift.com.br/license
 */
package br.com.bs.builders;

import br.com.bs.config.AppConfig;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author BlueShift
 */
@Component
public class RestServerBuilder extends RouteBuilder {
    
    @Autowired
    private AppConfig config;
    
    @Override
    public void configure() throws Exception {
        restConfiguration()
                .component("netty4-http")
                .componentProperty("throwExceptionOnFailure", "false")
                .host("0.0.0.0")
                .apiContextPath("/doc")
                .bindingMode(RestBindingMode.off)
                .dataFormatProperty("mustBeJAXBElement", "false")
                .port(config.getRestPort());
        
        rest("/admin")
                .get()
                .produces("application/json")
                .route()
                    .setBody().constant("{\"success\":true}");

    }

}
