/*
 * BlueShift intelectual property.
 * Contact the BlueShift team before use this source.
 * http://blueshift.com.br/license
 */
package br.com.bs.builders;

import br.com.bs.config.AppConfig;
import br.com.bs.config.Constants;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author BlueShift
 */
@Component
public class SchedulerBuilder extends RouteBuilder {
    
    @Autowired
    private AppConfig config;

    @Override
    public void configure() throws Exception {
        from("quartz2://scheduler/pooling?cron=0+*+*+*+*+?")
            .log(LoggingLevel.INFO , Constants.APP_LOG, "Running every minute...");
    }
    
}
