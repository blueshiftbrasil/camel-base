/*
 * BlueShift intelectual property.
 * Contact the BlueShift team before use this source.
 * http://blueshift.com.br/license
 */
package br.com.bs.config;

import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.jms.ConnectionFactory;
import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.camel.component.ActiveMQComponent;
import org.apache.activemq.pool.PooledConnectionFactory;
import org.apache.activemq.spring.ActiveMQConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.component.jms.JmsConfiguration;
import org.apache.camel.spring.boot.CamelContextConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author BlueShift
 */
@Configuration
@ComponentScan(basePackages = {Constants.APP_LOG})
@ConfigurationProperties(prefix = "app")
public class AppConfig {

    public static final Logger LOGGER = LoggerFactory.getLogger(AppConfig.class);

    @Autowired
    private CamelContext camelContext;

    private String pid;
   
    @Value("${app.redis.ip}")
    private String redisIp;

    @Value("${app.redis.port}")
    private int redisPort;

    @Value("${server-port}")
    private String serverPort;

    @Value("${management.port}")
    private String managementPort;

    @Value("${app.rest-port}")
    private String restPort;

    @Value("${app.activemq.broker-url}")
    private String mqBrokerURL;

    @Value("${app.activemq.pool-max-connections}")
    private int mqPoolMaxConnections;

    @Value("${app.activemq.concurrent-consumers}")
    private int mqConcurrentConsumers;
    
    @Value("${app.pidFile}")
    private String pidFile;
    
    private List<SomeObjectList> objectList = new ArrayList<>();
    
     public static class SomeObject {

        private String xxx;
        private String yyy;
        private String zzz;

        public String getXxx() {
            return xxx;
        }

        public void setXxx(String xxx) {
            this.xxx = xxx;
        }

        public String getYyy() {
            return yyy;
        }

        public void setYyy(String yyy) {
            this.yyy = yyy;
        }

        public String getZzz() {
            return zzz;
        }

        public void setZzz(String zzz) {
            this.zzz = zzz;
        }

    }

    public static class SomeObjectList {

        private SomeObject object1;
        private SomeObject object2;

        public SomeObject getObject1() {
            return object1;
        }

        public void setObject1(SomeObject object1) {
            this.object1 = object1;
        }

        public SomeObject getObject2() {
            return object2;
        }

        public void setObject2(SomeObject object2) {
            this.object2 = object2;
        }

    }

    public List<SomeObjectList> getObjectList() {
        return objectList;
    }

    public void setObjectList(List<SomeObjectList> objectList) {
        this.objectList = objectList;
    }

    public String getServerPort() {
        return serverPort;
    }

    public void setServerPort(String serverPort) {
        this.serverPort = serverPort;
    }

    public String getManagementPort() {
        return managementPort;
    }

    public void setManagementPort(String managementPort) {
        this.managementPort = managementPort;
    }

    public String getRedisIp() {
        return redisIp;
    }

    public void setRedisIp(String redisIp) {
        this.redisIp = redisIp;
    }

    public int getRedisPort() {
        return redisPort;
    }

    public void setRedisPort(int redisPort) {
        this.redisPort = redisPort;
    }

    public String getRestPort() {
        return restPort;
    }

    public void setRestPort(String restPort) {
        this.restPort = restPort;
    }

    public CamelContext getCamelContext() {
        return camelContext;
    }

    public void setCamelContext(CamelContext camelContext) {
        this.camelContext = camelContext;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getPidFile() {
        return pidFile;
    }

    public void setPidFile(String pidFile) {
        this.pidFile = pidFile;
    }

    public String getActiveMQBrokerURL() {
        return mqBrokerURL;
    }

    public void setActiveMQBrokerURL(String mqBrokerURL) {
        this.mqBrokerURL = mqBrokerURL;
    }

    public int getActiveMQPoolMaxConnections() {
        return mqPoolMaxConnections;
    }

    public void setActiveMQPoolMaxConnections(int mqPoolMaxConnections) {
        this.mqPoolMaxConnections = mqPoolMaxConnections;
    }

    public int getActiveMQConcurrentConsumers() {
        return mqConcurrentConsumers;
    }

    public void setActiveMQConcurrentConsumers(int mqConcurrentConsumers) {
        this.mqConcurrentConsumers = mqConcurrentConsumers;
    }

    @Bean
    public CamelContextConfiguration contextConfiguration() {
        return new CamelContextConfiguration() {
            @Override
            public void beforeApplicationStart(CamelContext context) {
                LOGGER.info("beforeApplicationStart...");
            }

            @Override
            public void afterApplicationStart(CamelContext arg0) {
                LOGGER.info("afterApplicationStart...");
            }
        };
    }

    @Bean
    public BrokerService broker() throws IOException {
        BrokerService service = new BrokerService();
        service.setPersistent(true);
        service.setUseShutdownHook(false);
        service.setTransportConnectorURIs(new String[]{this.mqBrokerURL});
        return service;
    }

    @Bean
    public ActiveMQComponent activeMQ() {
        ActiveMQComponent component = new ActiveMQComponent();
        component.setConfiguration(jmsConfig());
        return component;
    }

    @Bean
    public ConnectionFactory jmsConnectionFactory() {
        ActiveMQConnectionFactory activeMQFactory = new ActiveMQConnectionFactory();
        activeMQFactory.setBrokerURL(this.mqBrokerURL);

        PooledConnectionFactory pooledFactory = new PooledConnectionFactory();
        pooledFactory.setConnectionFactory(activeMQFactory);
        pooledFactory.setMaxConnections(this.mqPoolMaxConnections);

        return pooledFactory;
    }

    @Bean
    public JmsConfiguration jmsConfig() {
        JmsConfiguration configuration = new JmsConfiguration();
        configuration.setConnectionFactory(jmsConnectionFactory());
        configuration.setConcurrentConsumers(this.mqConcurrentConsumers);
        return configuration;
    }

    @PostConstruct
    public void onStart() {
        initializePID();

        LOGGER.info("Application PID: " + getPid());
        LOGGER.info("Rest port: " + getRestPort());
        LOGGER.info("Server port: " + getServerPort());
        LOGGER.info("Management port: " + getManagementPort());
        LOGGER.info("ActiveMQ URL: " + getActiveMQBrokerURL());
        LOGGER.info("Redis URL: " + getRedisIp() + ":" + getRestPort());

    }

    @PreDestroy
    public void onShutdown() {
        LOGGER.info("Application is shutting down...");
    }

    private void initializePID() {
        try {
            this.pid = ManagementFactory.getRuntimeMXBean().getName().split("@")[0];
            Path path = Paths.get(pidFile);
            Files.write(path, Arrays.asList(this.pid));
        } catch (IOException | RuntimeException ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
    }

}
