/*
 * BlueShift intelectual property.
 * Contact the BlueShift team before use this source.
 * http://blueshift.com.br/license
 */
package br.com.bs.processors;

import br.com.bs.config.Constants;
import java.util.UUID;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;

/**
 *
 * @author André Camillo
 */
public class GenerateUID implements Processor{

    @Override
    public void process(Exchange exchange) throws Exception {
        exchange.getIn().setHeader(Constants.UID, UUID.randomUUID().toString());
    }

}
