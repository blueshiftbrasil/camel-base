# README #

Projeto em Apache Camel sem nenhuma funcionalidade prática, porém pronto para uso como base em novos projetos.

### How do I get set up? ###

Esse projeto usa Maven, portanto tenha um Maven instalado ou uma boa IDE de desenvolvimento Java.
Após realizar o build de de seu aplicativo você poderá executá-lo da seguinte maneira:

```
#!shell


java -jar oscs-rest-0.2.jar --spring.config.location=file:application.yml
```
Ou com perfil de produção:

```
#!shell


java -jar oscs-rest-0.2.jar --spring.config.location=file:application.yml --spring.profiles.active=prod
```

### Referências ###
 * http://camel.apache.org/components.html
 * http://projects.spring.io/spring-boot/


### Enjoy!!! ###
